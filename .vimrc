" .vimrc ←↓↑→

" ignore case when tab completing file names
set wildignorecase

" set title in terminal window title bar
set title

" the last window will always have a status line
set laststatus=2
set statusline=%t[%{strlen(&fenc)?&fenc:'none'},%{&ff}]%h%m%r%y%=%c,%l/%L\ %P


set ai " autoindent

set sb " splitbelow

" doesn't seem needed anymore to get gnome-terminal transparency
"  change solid black background color to transparent in gnome-terminal
" autocmd vimenter * hi Normal guibg=NONE ctermbg=NONE


set et sts=4 sw=4 ts=4
"  et  = expandtab (spaces instead of tabs)
"  sts = softtabstop (the number of spaces to use when expanding tabs)
"  sw  = shiftwidth (the number of spaces to use when indenting)
"  ts  = tabstop (the number of spaces that a tab equates to)


" disable text wrapping
set textwidth=0
set wrap
" set formatoptions-=c

" enable mouse

"" unknown in nvim, vim only?
" set ttymouse=xterm2
set mouse=a


" automatically detect the type of file being edited, load filetype-specific
" plugins and apply indentation rules 
filetype plugin indent on

syntax on

source <sfile>:p:h/.vim/colorful-colors.vim

"colorscheme pablo
"colorscheme desert
"colorscheme dogerish " manually installed in ~/.vim/colors/dogerish.vim

" disable welcome text when starting vim
set shm+=I


" show stuff like "+y in the bottom right 
set showcmd

" show navigation coordinates in bottom right
set ruler 
set number
" toggle line numbers showing with control+n
" nnoremap <C-n> :set number!<CR>

function! CycleNumberModes()
    if &number && &relativenumber
        set nonumber norelativenumber
    elseif &number
        set relativenumber
    else
        set number
    endif
endfunction
nnoremap <C-n> :call CycleNumberModes()<CR>


" show carriage return and line feed chars
" e ++ff=unix | set list

" exit out of visual mode with escape without delay
set timeoutlen=1000
"set ttimeoutlen=0

" install from https://github.com/junegunn/vim-plug
" curl -fLo ~/.vim/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
call plug#begin('~/.vim/plugged')
" plugin to toggle comments with gc for selection or motion, gcc for single line, and gcgc for uncommenting comment block
Plug 'tpope/vim-commentary'
call plug#end()
" run :PlugInstall inside vim to install all plugins listed here

" removes the ~ characters at the end of buffer. (there is a trailing space after the \) 
set fillchars=eob:\ 
" achieve the same with neovims lua interface: (not here in .vimrc)
" vim.opt.fillchars = { eob = " " }

" also see ~/.config/nvim/init.vim



" The matchit plugin makes the % command work better, but it is not backwards
" compatible.
if has('syntax') && has('eval')
  packadd matchit
endif



" replace ':h ' with ':tab help ' for convenience so help files get openend up
" in a separate tab instead of in a window split as is default 
cnoreabbrev <expr> h getcmdtype() == ":" && getcmdline() == 'h' ? 'tab help' : 'h'

" :h persistent-undo
" https://sidneyliebrand.io/blog/vim-tip-persistent-undo
" guard for distributions lacking the persistent_undo feature.
if has('persistent_undo')
    " define a path to store persistent_undo files.
    let target_path = expand('~/.config/vim-persisted-undo/')

    " create the directory and any parent directories
    " if the location does not exist.
    if !isdirectory(target_path)
        call system('mkdir -p ' . target_path)
    endif

    " point Vim to the defined undo directory.
    let &undodir = target_path

    " finally, enable undo persistence.
    set undofile
endif

" Remember cursor position
if has("autocmd")
  au BufReadPost * if line("'\"") > 1 && line("'\"") <= line("$") | exe "normal! g'\"" | endif
endif

" center view when going up or down half a page
" nnoremap("<C-d>", "<C-d>zz")
" nnoremap("<C-u>", "<C-u>zz")
nnoremap <C-d> <C-d>zz
nnoremap <C-u> <C-u>zz

" set space as the leader key
let mapleader = " "
" space followed by p to replace visual selection while keeping yank buffer
xnoremap <leader>p "_dP

" add two terraform syntax files to ~/.vim/syntax/ dir
" https://www.reddit.com/r/vim/comments/wl29ng/syntax_highlighting_for_terraform_hcl/

" TODO
" version control with .bashrc, .tmux.conf
" paste/nopaste? 
" advanced substitute commands
" neovim vimtutor lvl 2